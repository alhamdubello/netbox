#!/bin/bash
#Author: Alhamdu Bello
#Date: 14 September 2022
#Title: startup script netbox install
###############################################
set -x

ROOT_SETUP_DIR=$PWD
apt update -y
apt install ansible net-tools -y
apt install gcc make dkms libelf-dev -y
apt update -y
echo "Installing Depedancy Roles and Collections"
ansible-galaxy collection install -r $ROOT_SETUP_DIR/collections/requirements.yml -i -c
ansible-play site.yml
#
